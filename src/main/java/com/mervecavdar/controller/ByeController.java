package com.mervecavdar.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bye")
public class ByeController {

    @GetMapping(value = "/say")
    @ApiOperation("Used to say bye.")
    public String say() {
        return "Bye";
    }

    @PostMapping(value = "/sayByName")
    @ApiOperation("Used to say bye by name.")
    public String sayByName(@RequestParam String name) {
        return "Bye " + name;
    }

}
