package com.mervecavdar.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hello")
public class HelloController {

    @GetMapping(value = "/say")
    @ApiOperation("Used to say hello.")
    public String say() {
        return "Hello";
    }

    @PostMapping(value = "/sayByName")
    @ApiOperation("Used to say hello by name.")
    public String sayByName(@RequestParam String name) {
        return "Hello " + name;
    }

}
